import getpass
import sys
import requests
import random

from locust import HttpLocust, TaskSet, task
from requests.auth import HTTPBasicAuth

HOST = "https://prodportal.solaranalytics.com"
USR = 'alec@solaranalytics.com.au'
PASS = getpass.getpass('Password for {0}: '.format(USR))
TOKEN = None

SITES = ['140', '21383', '7401', '20329', '22390', '7041', '23242', '7316', '7221']


def get_token():
    response = requests.get(HOST + '/api/v2/token', auth=(USR, PASS))
    if response.status_code != 200:
        print (('ERROR Unable to get token for user: {0}\n'
                'status code {1} \n'
                 'Response: {2}').format(USR,
                                         response.status_code,
                                         response.text))
    else:
        global TOKEN
        TOKEN = response.json()['token']


get_token()
if TOKEN is None:
    print '\033[91mInvalid credentials supplied\033[0m'
    sys.exit(1)



class UserBehavior(TaskSet):

    def on_start(self):
        '''
            Set the the Authorization header
        '''
        self.client.auth = HTTPBasicAuth(TOKEN, 'x')

    @task(1)
    def get_sydney_weather(self):
        self.client.get("/api/v3/current_weather?country=au&postcode=2000")   

    @task(3)
    def get_site_data(self):
        self.client.get("/api/v2/site_data/{0}".format(
            random.choice(SITES)))    

    @task(3)
    def get_live_site_data(self):
        self.client.get("/api/v2/live_site_data/{0}".format(
            random.choice(SITES)))  

    @task(2)
    def get_site_list(self):
        ''' Get List of AGL sites '''
        self.client.get("/api/v2/site_list/561")


class WebsiteUser(HttpLocust):
    host = HOST
    task_set = UserBehavior
    min_wait = 1000
    max_wait = 2000
